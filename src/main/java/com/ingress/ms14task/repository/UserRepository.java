package com.ingress.ms14task.repository;

import com.ingress.ms14task.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    boolean existsByMail(String email);

    User findByUserName(String userName);

    User findByMail(String email);
}
