package com.ingress.ms14task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ms14TaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ms14TaskApplication.class, args);
    }

}
