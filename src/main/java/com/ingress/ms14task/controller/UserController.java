package com.ingress.ms14task.controller;

import com.ingress.ms14task.dto.LoginDto;
import com.ingress.ms14task.dto.ResetPasswordDto;
import com.ingress.ms14task.dto.UserRegisterDto;
import com.ingress.ms14task.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping("/register")
    public ResponseEntity<String> registerUser(@RequestBody UserRegisterDto userDto) {

        userService.register(userDto);

        return new ResponseEntity<>("User registered successfully", HttpStatus.CREATED);
    }

    @PostMapping("/login")
    public ResponseEntity<String> loginUser(@RequestBody LoginDto loginDto) {

        if (userService.login(loginDto)) {
            return new ResponseEntity<>("User login successfully", HttpStatus.CREATED);

        }

        return new ResponseEntity<>("Password or username is incorrect", HttpStatus.UNAUTHORIZED);
    }

    @PostMapping("/reset")
    public ResponseEntity<String> resetPassword(@RequestBody ResetPasswordDto resetPasswordDto, @RequestParam("email") String email) {
        userService.resetPassword(resetPasswordDto, email);

        return new ResponseEntity<>("Password reset successfully", HttpStatus.CREATED);
    }


}
