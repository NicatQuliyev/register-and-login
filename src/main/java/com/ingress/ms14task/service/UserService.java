package com.ingress.ms14task.service;

import com.ingress.ms14task.dto.LoginDto;
import com.ingress.ms14task.dto.ResetPasswordDto;
import com.ingress.ms14task.dto.UserRegisterDto;
import com.ingress.ms14task.entity.User;
import com.ingress.ms14task.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    private final ModelMapper modelMapper;

    public void register(UserRegisterDto userDto) {
        User user = modelMapper.map(userDto, User.class);

        if (!user.getPassword().equals(user.getRepeatPassword())) {
            throw new RuntimeException("Repeat Password Correctly");
        }

        if (userRepository.existsByMail(userDto.getEmail())) {
            throw new RuntimeException("Email Already exists");
        }

        userRepository.save(user);

    }

    public boolean login(LoginDto loginDto) {
        User user = userRepository.findByUserName(loginDto.getUserName());

        if (user == null) {
            throw new RuntimeException("Not found username " + loginDto.getUserName());
        }

        return user.getPassword().equals(loginDto.getPassword());
    }

    public void resetPassword(ResetPasswordDto resetPasswordDto,String email) {
        User user = userRepository.findByMail(resetPasswordDto.getEmail());

        if (!user.getMail().equals(email)) {
            throw new RuntimeException("Invalid email");
        }

        user.setPassword(resetPasswordDto.getNewPassword());

        userRepository.save(user);
    }

}
