FROM openjdk:17-oracle
COPY build/libs/ms14-task-0.0.1-SNAPSHOT.jar /ms14-app/
CMD ["java","-jar","/ms14-app/ms14-task-0.0.1-SNAPSHOT.jar"]
